#!/bin/bash
ip=$(curl -sk https://api.ipify.org)
echo "Updating Dynamic DNS to: $ip"
resp=$(curl -sk http://freedns.afraid.org/dynamic/update.php?${APIKEY})
echo $resp
