FROM ubuntu:latest
MAINTAINER docker@vrelk.com
 
# Add crontab file in the cron directory
ADD crontab /etc/cron.d/afraid-cron

# Add updater script
ADD updater.sh /updater.sh

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/afraid-cron

#Install Cron
RUN apt-get update
RUN apt-get -y install cron


# Run the command on container startup
CMD cron